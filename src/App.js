import React, { createContext, useContext, useState } from "react";
import RoutesContainer from "./router/RoutesContainer";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { I18nextProvider } from "react-i18next";
import i18n from "./translation/i18n";
import Footer from "./components/Footer";
import "./App.css";

const CardsContext = createContext();

const queryClient = new QueryClient();

function App() {
  const [data, setData] = useState(null);

  return (
    <div className="App">
      <I18nextProvider i18n={i18n}>
        <CardsContext.Provider value={{ data, setData }}>
          <QueryClientProvider client={queryClient}>
            <RoutesContainer />
          </QueryClientProvider>
        </CardsContext.Provider>
        <Footer />
      </I18nextProvider>
    </div>
  );
}

export const useCardContext = () => useContext(CardsContext);

export default App;
