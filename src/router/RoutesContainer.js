import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Cards from "../pages/Cards";
import Header from "../components/Header";
import Tables from "../pages/Tables";

const RoutesContainer = () => {
  return (
    <>
      <Router>
        <Header />
        <Routes>
          <Route exact path="/" element={<Cards />} />
          <Route exact path="/tables" element={<Tables />} />
        </Routes>
      </Router>
    </>
  );
};

export default RoutesContainer;
