import React, { useState, useCallback } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import { useCardContext } from "../App";

const styles = {
  tablesContainer: {
    display: "grid",
    gridAutoFlow: "row",
    justifyContent: "center",
    alignItems: "center",
    gap: "54px",
  },
  counterContainer: {
    display: "grid",
    gridAutoFlow: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  operatorBtn: {
    backgroundColor: "transparent",
    border: "none",
    cursor: "pointer",
  },
  inputCounter: {
    color: "#454545",
    backgroundColor: "transparent",
    fontSize: "larger",
    border: "none",
    textAlign: "center",
  },
  iconColor: { color: "#454545" },
  sendBtn: {
    backgroundColor: "#252626c4",
    borderRadius: "25px",
    padding: "8px",
    border: "none",
    cursor: "pointer",
    display: "grid",
    gridAutoFlow: "column",
    justifyContent: "center",
    alignItems: "center",
    gap: "13px",
    color: "whitesmoke",
  },
  sendContainer: {
    display: "grid",
    gridAutoFlow: "column",
    justifyContent: "center",
    alignItems: "end",
    gap: "21px",
  },
  generatedTablesCont: {
    display: "grid",
    gridTemplateColumns: "repeat(4, 1fr)",
    gridTemplateRows: "repeat(4, 1fr)",
    justifyItems: "center",
    gap: "10px",
    width: "50%",
    padding: "21px",
    borderRadius: "15px",
  },
  imgCardTable: {
    borderRadius: "10px",
    boxShadow: "rgb(45 45 46 / 94%) 0px 7px 29px 0px",
  },
};

function Tables() {
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);
  const { t } = useTranslation();
  const { data } = useCardContext();
  const [number, setNumber] = useState(0);
  const [tablesSet, setTablesSet] = useState(null);

  const generateTable = useCallback(() => {
    const tablesCont = [];
    const cardsCont = data;

    for (let j = 1; j <= number; j++) {
      for (let i = cardsCont.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [cardsCont[i], cardsCont[j]] = [cardsCont[j], cardsCont[i]];
      }
      tablesCont.push(cardsCont.slice(0, 16));
    }

    setTablesSet(tablesCont);
    setIsLoading(false);
  }, [data, number]);

  return (
    <>
      <button
        onClick={() => navigate("/")}
        style={{ ...styles.sendBtn, marginLeft: "21px" }}
      >
        <span class="material-symbols-outlined">keyboard_return</span>
      </button>

      {data ? (
        <>
          <div style={styles.tablesContainer}>
            <div style={styles.iconColor}>
              <p>{t("number-tables")}</p>
              <div style={styles.counterContainer}>
                <button
                  disabled={number === 0}
                  onClick={() => setNumber((prevNumber) => prevNumber - 1)}
                  style={styles.operatorBtn}
                >
                  <span
                    className="material-symbols-outlined"
                    style={styles.iconColor}
                  >
                    remove
                  </span>
                </button>
                <input
                  type="number"
                  value={number}
                  readOnly
                  style={styles.inputCounter}
                />
                <button
                  onClick={() => setNumber((prevNumber) => prevNumber + 1)}
                  style={styles.operatorBtn}
                >
                  <span
                    className="material-symbols-outlined"
                    style={styles.iconColor}
                  >
                    add
                  </span>
                </button>
              </div>
            </div>
            <div style={styles.sendContainer}>
              <button
                onClick={() => {
                  setIsLoading(true);
                  generateTable();
                }}
                disabled={isLoading}
                style={styles.sendBtn}
              >
                {t("generate")}
                <span
                  className="material-symbols-outlined"
                  style={{ color: "whitesmoke" }}
                >
                  send
                </span>
              </button>
            </div>
          </div>
          <div>
            <div className="table-container">
              {tablesSet
                ? tablesSet.map((cards, i) => (
                    <div style={styles.generatedTablesCont} key={i}>
                      {cards.map((x) => (
                        <div key={x.name + x.id + i}>
                          <img
                            src={x.image}
                            width={50}
                            height={70}
                            style={styles.imgCardTable}
                            alt={x.name}
                          />
                        </div>
                      ))}
                    </div>
                  ))
                : null}
              {isLoading ? "loading..." : null}
            </div>
          </div>
        </>
      ) : (
        t("no-data")
      )}
    </>
  );
}

export default Tables;
