import React, { useEffect } from "react";
import { useQuery } from "@tanstack/react-query";
import { useTranslation } from "react-i18next";
import { useCardContext } from "../App";

const styles = {
  imgCard: {
    borderRadius: "10px",
    boxShadow: "rgb(45 45 46 / 94%) 0px 7px 29px 0px",
  },
};

function Cards() {
  const { t } = useTranslation();
  const { setData } = useCardContext();
  const { isPending, error, data } = useQuery({
    queryKey: ["loteryCards"],
    queryFn: () =>
      fetch(
        `${process.env.REACT_APP_API_URL}:${process.env.REACT_APP_API_PORT}/cards/`
      ).then((res) => res.json()),
  });

  useEffect(() => {
    if (data && !error && !isPending) {
      setData(data);
    }
  }, [data, error, isPending, setData]);

  if (isPending) return "Loading...";

  if (error) return t("no-data");
  return (
    <>
      {
        <div className="card-container">
          {data.map((x) => (
            <div key={x.name + x.item}>
              <img
                alt={x.name}
                src={x.image}
                width={150}
                height={200}
                style={styles.imgCard}
              />
            </div>
          ))}
        </div>
      }
    </>
  );
}

export default Cards;
