import React from "react";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";

const styles = {
  titleContainer: {
    textAlign: "start",
    marginLeft: "21px",
  },
  headerContainer: {
    display: "grid",
    gridAutoFlow: "column",
    justifyContent: "flex-end",
    alignItems: "center",
    marginRight: "21px",
    gap: "21px",
  },
  btnNav: {
    display: "grid",
    gridAutoFlow: "column",
    justifyContent: "center",
    alignItems: "center",
    gap: "8px",
    width: "fit-content",
    height: "fit-content",
    color: "whitesmoke",
    backgroundColor: "transparent",
    border: "none",
    cursor: "pointer",
  },
};

function Header() {
  const navigate = useNavigate();
  const { t } = useTranslation();
  return (
    <div className="header">
      <p style={styles.titleContainer}>{t("title")}</p>
      <div style={styles.headerContainer}>
        <button style={styles.btnNav} onClick={() => navigate("/")}>
          <p>{t("card-list")}</p>
          <span className="material-symbols-outlined">dashboard</span>
        </button>
        <button style={styles.btnNav} onClick={() => navigate("/tables")}>
          <p>{t("tables-generator")}</p>
          <span className="material-symbols-outlined">draw</span>
        </button>
      </div>
    </div>
  );
}

export default Header;
