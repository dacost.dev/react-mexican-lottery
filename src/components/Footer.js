import React from "react";
import { useTranslation } from "react-i18next";

const styles = {
  btnLang: {
    backgroundColor: "transparent",
    border: "none",
    cursor: "pointer",
  },
};

function Footer() {
  const { i18n } = useTranslation();

  return (
    <div className="footer">
      <div style={{ marginRight: "21px" }}>
        <button
          style={styles.btnLang}
          onClick={() => i18n.changeLanguage("es")}
        >
          <img
            src="https://i.pinimg.com/736x/c0/fe/9a/c0fe9ab31c1b165ec774b49f3d3a145c.jpg"
            width={21}
            height={13}
            alt="Español"
          />
        </button>
        <button
          style={styles.btnLang}
          onClick={() => i18n.changeLanguage("en")}
        >
          <img
            src="https://i.pinimg.com/736x/a7/f6/66/a7f666d897fa60fe0c3681f151fa7fb1.jpg"
            width={21}
            height={13}
            alt="English"
          />
        </button>
      </div>
    </div>
  );
}

export default Footer;
