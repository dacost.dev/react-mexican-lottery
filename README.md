# REACT MEXICAN LOTTERY GENERATOR 

![Imagen 1](./src/assets/cardsPage.png)
_Listado de cartas._

![Imagen 2](./src/assets/tablesPage.png)
_Generador de tablas._

## Generador de Tablas de Jugador de Lotería Mexicana 

Este proyecto consiste en un generador de tablas de jugador de la lotería mexicana. Se implementa el algoritmo de Fisher-Yates para garantizar que no haya dos o más cartas repetidas en una tabla. Además, se asegura que ninguna de las tablas generadas sea igual utilizando combinaciones de elementos.

## Algoritmo de Fisher-Yates

El algoritmo de Fisher-Yates es un algoritmo para generar una permutación aleatoria de un conjunto finito de elementos. Se utiliza en este proyecto para garantizar que las tablas generadas no tengan cartas repetidas.

## Fórmula para Calcular el Número de Combinaciones

La fórmula utilizada para calcular el número de combinaciones posibles sin repetición de \( m \) elementos tomados de un conjunto de \( n \) elementos es:

C(n, m) = n! / (m! * (n - m)!) 

Esto asegura que ninguna de las tablas generadas sea igual.

La fórmula específica para este proyecto, con \( n = 54 \) cartas y \( m = 16 \) referencias por tabla, es:

54! / (16! * (54 - 16)!)

Esto garantiza la variedad y unicidad de las tablas generadas.

## Instalación y Ejecución

Para instalar las dependencias del proyecto, sigue estos pasos:

1. Clona el repositorio en tu máquina local.

```sh
git clone git@gitlab.com:dacost.dev/react-mexican-lottery.git
```

2. Ejecuta el comando para instalar todas las dependencias.

```sh
yarn install
```

3. Ejecuta la app

```sh
yarn start
```

## Base de Datos y Backend

El script de la base de datos, además, de el backend de la aplicación está disponible en [este repositorio](https://gitlab.com/dacost.dev/fastapi-backend-lottery).

## Licencia

Este proyecto está bajo la Licencia MIT. Para más detalles, consulta el archivo LICENSE.md.